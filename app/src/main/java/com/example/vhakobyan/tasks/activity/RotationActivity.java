package com.example.vhakobyan.tasks.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.RotateAnimation;
import android.widget.TextView;

import com.example.vhakobyan.tasks.R;

public class RotationActivity extends BaseActivity {

    private View myRectangleView;
    private TextView textView;
    private float initialX;
    private DisplayMetrics displayMetrics;

    private double mCurrAngle = 0;
    private double mPrevAngle = 0;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rotation);

        textView = (TextView) findViewById(R.id.textView);
        myRectangleView = findViewById(R.id.myRectangleView);

        displayMetrics = getResources().getDisplayMetrics();
        textView.setText("riotation angle is = " + 0);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                initialX = event.getX();
                break;
            case MotionEvent.ACTION_MOVE:
                float dx = initialX - event.getX();
                mPrevAngle = mCurrAngle;
                mCurrAngle = Math.toDegrees(Math.atan2(displayMetrics.widthPixels / 2, dx));
                animate(mPrevAngle, mCurrAngle, 0);
                break;
        }
        return super.onTouchEvent(event);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (KeyEvent.KEYCODE_BACK == keyCode) {
            Intent mainIntent = new Intent(RotationActivity.this, MainActivity.class);
            RotationActivity.this.startActivity(mainIntent);
            RotationActivity.this.finish();
        }
        return super.onKeyDown(keyCode, event);
    }

    private void animate(double fromDegrees, double toDegrees, long durationMillis) {
        final RotateAnimation rotate = new RotateAnimation((float) fromDegrees, (float) toDegrees,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f,
                RotateAnimation.RELATIVE_TO_SELF, 0.5f);
        rotate.setDuration(durationMillis);
        rotate.setFillEnabled(true);
        rotate.setFillAfter(true);
        myRectangleView.startAnimation(rotate);
        int angle = (int) (mPrevAngle - mCurrAngle) * 180;
        textView.setText("riotation angle is = " + angle);
    }

}
