package com.example.vhakobyan.tasks.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.example.vhakobyan.tasks.R;
import com.example.vhakobyan.tasks.controller.WeatherRequestController;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.MessageFormat;


public class WeatherActivity extends BaseActivity {

    public static final String TAG = MainActivity.class.getSimpleName();

    public static final String BASE_URL = "http://api.openweathermap.org/data/2.5/weather?q={0}&appid=2156e2dd5b92590ab69c0ae1b2d24586&units=metric";

    private Spinner spinner;
    private TextView temperature, description, country;
    private JSONObject weather;


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_weather);

        spinner = (Spinner) findViewById(R.id.spinner);
        temperature = (TextView) findViewById(R.id.temperature);
        country = (TextView) findViewById(R.id.country);
        description = (TextView) findViewById(R.id.description);

        final String[] data = {"china", "usa", "brazil", "korea"};

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_spinner_item, data);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setPrompt("Country");

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                Toast.makeText(getBaseContext(), "Position = " + position, Toast.LENGTH_SHORT).show();
                if (isConnected()) {
                    makeRequest();
                } else {
                    Toast.makeText(getApplicationContext(), "Please check your Internet connection", Toast.LENGTH_LONG).show();
                }
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                spinner.setPrompt("Country");
            }
        });



    }

    private void makeRequest() {

        String currentUrl = MessageFormat.format(BASE_URL, spinner.getSelectedItem().toString());
        Log.i(TAG, currentUrl);

        JsonObjectRequest jsonObjReq = new JsonObjectRequest(
                Request.Method.GET, currentUrl, null, new Response.Listener<JSONObject>() {

            @Override
            public void onResponse(JSONObject response) {

                try {

                    Log.i(TAG, response.toString());

                    weather = (JSONObject) response.getJSONArray("weather").get(0);
                    JSONObject main = response.getJSONObject("main");
                    description.setText(weather.get("main").toString());
                    temperature.setText(main.get("temp").toString()+ " °C");
                    country.setText(response.getString("name"));
                    Log.i (TAG, response.toString());

                } catch (JSONException e) {
                    Log.e(TAG, "Error: " + e.getMessage());
                    Toast.makeText(getApplicationContext(), "Error , try again ! ", Toast.LENGTH_LONG).show();
                }
            }

        }, new Response.ErrorListener() {

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e(TAG, "Error: " + error.getMessage());
                Toast.makeText(getApplicationContext(), "Error while loading ... ", Toast.LENGTH_SHORT).show();
            }
        });

        // Adding request to request queue
        WeatherRequestController.getInstance(getApplicationContext()).addToRequestQueue(jsonObjReq);
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (KeyEvent.KEYCODE_BACK == keyCode) {
            Intent mainIntent = new Intent(WeatherActivity.this, MainActivity.class);
            WeatherActivity.this.startActivity(mainIntent);
            WeatherActivity.this.finish();
        }
        return super.onKeyDown(keyCode, event);
    }

}


